# PERMISSIVE #



### What is PERMISSIVE? ###

**PERMISSIVE** (**P**rivate **E**mail **R**oles & **M**essaging **I**n **S**ecure **S**ocial Networks via **I**nvitation and **VE**rification) 
is a decentralized messaging and social platform that allows emails and private communication between individuals in a secure environment.